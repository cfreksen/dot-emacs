;;-*-coding: utf-8;-*-
(define-abbrev-table 'Buffer-menu-mode-abbrev-table '())

(define-abbrev-table 'Custom-mode-abbrev-table '())

(define-abbrev-table 'comint-mode-abbrev-table '())

(define-abbrev-table 'completion-list-mode-abbrev-table '())

(define-abbrev-table 'coq-goals-mode-abbrev-table '())

(define-abbrev-table 'coq-mode-abbrev-table
  '(
    ("id" "induction" nil 0)
    ("re" "reflexivity." nil 0)
    ("si" "simpl." nil 0)
   ))

(define-abbrev-table 'coq-response-mode-abbrev-table '())

(define-abbrev-table 'coq-shell-mode-abbrev-table '())

(define-abbrev-table 'dsssl-mode-abbrev-table '())

(define-abbrev-table 'edit-abbrevs-mode-abbrev-table '())

(define-abbrev-table 'emacs-lisp-byte-code-mode-abbrev-table '())

(define-abbrev-table 'emacs-lisp-mode-abbrev-table '())

(define-abbrev-table 'fundamental-mode-abbrev-table '())

(define-abbrev-table 'global-abbrev-table '())

(define-abbrev-table 'help-mode-abbrev-table '())

(define-abbrev-table 'inferior-python-mode-abbrev-table '())

(define-abbrev-table 'lisp-mode-abbrev-table '())

(define-abbrev-table 'messages-buffer-mode-abbrev-table '())

(define-abbrev-table 'occur-edit-mode-abbrev-table '())

(define-abbrev-table 'occur-mode-abbrev-table '())

(define-abbrev-table 'process-menu-mode-abbrev-table '())

(define-abbrev-table 'prog-mode-abbrev-table '())

(define-abbrev-table 'proof-goals-mode-abbrev-table '())

(define-abbrev-table 'proof-mode-abbrev-table '())

(define-abbrev-table 'proof-response-mode-abbrev-table '())

(define-abbrev-table 'proof-shell-mode-abbrev-table '())

(define-abbrev-table 'proof-splash-mode-abbrev-table '())

(define-abbrev-table 'proof-universal-keys-only-mode-abbrev-table '())

(define-abbrev-table 'python-mode-abbrev-table '())

(define-abbrev-table 'python-mode-skeleton-abbrev-table
  '(
   ))

(define-abbrev-table 'scheme-mode-abbrev-table '())

(define-abbrev-table 'scomint-mode-abbrev-table '())

(define-abbrev-table 'special-mode-abbrev-table '())

(define-abbrev-table 'tabulated-list-mode-abbrev-table '())

(define-abbrev-table 'text-mode-abbrev-table '())

