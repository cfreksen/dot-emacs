;; Add language support here
;;

(add-to-list 'load-path "~/.emacs.d/elisp/language-modes/")

;; Coq
(if --loc-coq?
    (progn
      (load-file "C:/Programmer (alt)/Proof General 4.2/generic/proof-site.el")
      (add-to-list 'load-path "C:/Programmer (alt)/Proof General 4.2/coq"))
  ())

(add-hook 'proof-ready-for-assistant-hook (lambda () (show-paren-mode 0))) ; ??
(setq proof-splash-seen t)                    ; Removs splash screen
(setq proof-three-window-mode-policy 'hybrid) ; Forces Two column split of buffers
(setq proof-script-fly-past-comments t)       ; Jumps over multiple comments in one go

(with-eval-after-load 'coq
  (define-key coq-mode-map "\M-n"            ; Evaluates next statement/
    #'proof-assert-next-command-interactive) ; Command. Alt: C-c C-n.
  (define-key coq-mode-map "\M-e" nil)       ; Restores M-a and M-e
  (define-key coq-mode-map "\M-a" nil))      ; navigation.

(define-abbrev-table 'coq-mode-abbrev-table '())          ; Abbr.
(define-abbrev coq-mode-abbrev-table "re" "reflexivity.") ; for common
(define-abbrev coq-mode-abbrev-table "id" "induction")    ; tacticts.
(define-abbrev coq-mode-abbrev-table "si" "simpl.")       ;
(advice-add 'proof-assert-next-command-interactive        ; Expand
            :before #'expand-abbrev)                      ; when eval.

;; Scheme
(if --loc-scheme?
    (progn
      (setq scheme-program-name --loc-scheme-location)
      (load-library             "scheme-setup"))
  ())

;; OCaml
(add-to-list 'load-path "~/.emacs.d/elisp/language-modes/Tuareg 2.0.9/")
(load "~/.emacs.d/elisp/language-modes/Tuareg 2.0.9/tuareg-site-file")

;; Tiger
(autoload 'tiger-mode "tiger" "Load tiger-mode" t)
(add-to-list 'auto-mode-alist '("\\.ti[gh]$" . tiger-mode))

;; Python
;; (add-to-list 'load-path "~/.emacs.d/elisp/language-modes/python-mode.el-6.2.0/")
;; (autoload 'python-mode "python-mode" "Python Mode." t)
;; (add-to-list 'auto-mode-alist '("\\.py\\'" . python-mode))
;; (add-to-list 'interpreter-mode-alist '("python" . python-mode))

;; Javascript
(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))
;; TODO: Skewer seems to make Emacs hang on close
;; (add-hook 'js2-mode-hook 'skewer-mode)
;; (add-hook 'css-mode-hook 'skewer-css-mode)
;; (add-hook 'html-mode-hook 'skewer-html-mode)

;; ;; Seems dangerous
;; (require 'simple-httpd)
;; (setq httpd-port 8877)
;; (setq httpd-root "c:/Users/cbf_x_000/git/awp")

;; Org-mode
(defun org-summary-todo (n-done n-not-done)
  "Switch entry to DONE when all subentries are done, to TODO otherwise."
  (let (org-log-done org-log-states)   ; turn off logging
    (org-todo (if (= n-not-done 0) "DONE" "TODO"))))

(add-hook 'org-after-todo-statistics-hook 'org-summary-todo)

;; Add WIP state to TODO/DONE
(setq org-todo-keywords '((sequence "TODO" "WIP" "DONE")))
(setq org-todo-keyword-faces '(("WIP" . "orange")))


(require 'ox-gfm)

;; C/C++
(setq-default c-basic-offset 4)

;; ATS
(add-to-list 'load-path "~/.emacs.d/elisp/language-modes/ats/")
;; Hacky
(setq flycheck-ats2-executeable "/home/ats/ATS2-Postiats-0.2.4/bin/patscc")
(load-library "ats2-mode")

(setenv "ATSHOME" "/home/ats/ATS2-Postiats-0.2.4")
(setenv "PATSHOME" "/home/ats/ATS2-Postiats-0.2.4")
(setenv "PATH" (concat "/home/ats/ATS2-Postiats-0.2.4/bin" path-separator
                       (getenv "PATH")))
(load-library "flycheck-ats2")

(with-eval-after-load 'flycheck
  (flycheck-ats2-setup))

(add-to-list 'auto-minor-mode-alist '("\\.\\(s\\|d\\|h\\)ats\\'" . flycheck-mode)) ; Add flycheck to ats mode
(add-to-list 'auto-mode-alist '("\\.\\(s\\|d\\|h\\)ats\\'" . ats-mode))

;; Jif
(add-to-list 'auto-mode-alist '("\\.jif$" . java-mode))

;; LaTeX
(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)

;; Additional fold-tex targets

;; Collapse items until point is over them, like footnotes
(add-hook 'LaTeX-mode-hook (lambda ()
                             (progn
                               ;; Additional LaTeX macros to fold
                               (add-to-list 'LaTeX-fold-macro-spec-list '("[TODO]" ("todo")))
                               (add-to-list 'LaTeX-fold-macro-spec-list '("[r]" ("autoref")))
                               ;; Enable folding
                               (TeX-fold-mode 1)
                               ;; Fold the opened document
                               (TeX-fold-buffer))))
;; (setq TeX-fold-auto t)

;; Use reftex to handle references, citations etc.
(require 'reftex)
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
(setq reftex-ref-style-default-list '("Default" "Cleveref"))
(setq reftex-plug-into-AUCTeX t)
(setq reftex-cite-format 'natbib)

;; SML
(if --loc-sml?
    (progn
      (require 'sml-mode)
      (add-to-list 'auto-mode-alist '("\\.\\(sml\\|sig\\)\\'" . sml-mode))
      (setq sml-program-name "C:/Program Files (x86)/SMLNJ/bin/sml.bat"))
  ())
