;; GUI
(setq inhibit-startup-message t)  ; dont show the GNU splash screen
(setq initial-scratch-message ";; Emacs\n\n")
(line-number-mode 1)              ; line number
(column-number-mode 1)            ; column number
(setq frame-title-format "%b")    ; titlebar shows buffer's name
(tool-bar-mode -1)                ; remove tool bar
(menu-bar-mode -1)                ; remove menu bar
(scroll-bar-mode -1)              ; remove scroll bars (time to grow up)
(setq display-time-24hr-format 1) ; Clock should be 24h clock
(setq display-time-default-load-average nil) ; Do not display system load next to the clock
(display-time-mode 1)             ; Clock in the mode line


;; Theme
(set-face-attribute 'default nil :font "DejaVu Sans Mono-10") ; Set font


