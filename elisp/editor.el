;; Initial folder
(cd --loc-start-dir)

;; Behaviour
(show-paren-mode t)                 ; turn on highlight paren mode
(fset 'yes-or-no-p 'y-or-n-p)       ; use y and n for questions
(setq visible-bell 1)               ; turn off bip warnings
(transient-mark-mode t)             ; show selection from mark
(global-font-lock-mode t)           ; enable syntax highlighting
(setq-default show-trailing-whitespace t) ; To show trailing whitespace in all buffers

;; IDO
(require 'ido)                      ; to get better buffer switching
(ido-mode t)                        ; and file opening
(setq ido-auto-merge-work-directories-length -1) ; Prevent IDO from looking in other dirs


;; Backupspp
(defvar --backup-directory (concat user-emacs-directory "backups"))
(if (not (file-exists-p --backup-directory))
        (make-directory --backup-directory t))
(setq backup-directory-alist `(("." . ,--backup-directory)))
(setq make-backup-files t               ; backup of a file the first time it is saved.
      backup-by-copying t               ; don't clobber symlinks
      version-control t                 ; version numbers for backup files
      delete-old-versions t             ; delete excess backup files silently
      delete-by-moving-to-trash t
      kept-old-versions 6               ; oldest versions to keep when a new numbered backup is made (default: 2)
      kept-new-versions 9               ; newest versions to keep when a new numbered backup is made (default: 2)
      auto-save-default t               ; auto-save every buffer that visits a file
      auto-save-timeout 20              ; number of seconds idle time before auto-save (default: 30)
      auto-save-interval 200            ; number of keystrokes between auto-saves (default: 300)
      )

;; Typing
(setq-default indent-tabs-mode nil) ; nil => don't use tabs to indent
(setq default-tab-width 4)          ; Tab width

;; UTF-8
(set-language-environment "UTF-8")
(setq locale-coding-system 'utf-8)
(set-selection-coding-system 'latin-1)  ; cannot paste æøå from outside otherwise
(prefer-coding-system 'utf-8)

;; SSH access through TRAMP
(setenv "PATH" (concat "c:/Programmer (alt)/Putty/;" (getenv "PATH")))
(defun fh ()
  "SSH into freshhorse"
  (interactive)
  (find-file "/plink:fh:/"))

;; MELPA
(when (>= emacs-major-version 24)
  (require 'package)
  (add-to-list
   'package-archives
   '("melpa" . "http://melpa.org/packages/")
   t)
  (package-initialize))

;; Presentation
(require 'command-log-mode)


;; Buffer switching
(global-set-key (kbd "M-o")  'mode-line-other-buffer)
