(defconst --loc-start-dir ".")

;; Coq
(defconst --loc-coq? nil)

;; Scheme
(defconst --loc-scheme? nil)

(defconst --loc-scheme-location nil)

;; SML
(defconst --loc-sml? nil)
