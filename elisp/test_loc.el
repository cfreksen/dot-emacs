(if (boundp '--loc-start-dir)
    ()
  (message "--loc-start-dir not bound\n"))

;; Coq
(if (boundp '--loc-coq?)
    ()
  (message "--loc-coq? not bound\n"))

;; Scheme
(if (boundp '--loc-scheme?)
    ()
  (message "--loc-scheme? not bound\n"))

(if (boundp '--loc-scheme-location)
    ()
  (message "--loc-scheme-location not bound\n"))

;; SML
(if (boundp '--loc-sml?)
    ()
  (message "--loc-sml? not bound\n"))
